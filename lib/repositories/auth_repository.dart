import 'package:Provider_template_v1/data/dto/login_dto.dart';
import 'package:Provider_template_v1/data/models/user.dart';
import 'package:Provider_template_v1/services/auth_service.dart';

class AuthRepository {
  // region [Initialize]
  static const String TAG = '[RouteHelper]';

  AuthRepository._privateConstructor();

  static final AuthRepository _instance = AuthRepository._privateConstructor();

  static AuthRepository get instance => _instance;

  // endregion

  Future<User> loginWithEmailAndPassword(LoginDto dto) async {
    return User.fromMap(
      await AuthService.instance.loginWithEmailAndPassword(dto),
    );
  }
}
